Feature: End to end API Test for Book Store API

  Background: User generates token for Authorization
    Given I am an authorized user "TestUser1" "Password_1!"

  @end2end
  Scenario Outline: Authorized user is able to Add and Remove a book.
    Given A list of books are available
    When I add a book with isbn "<isbn>" to my reading list with userId "<userid>"
    Then The book with isbn "<isbn>" is added to account with userId "<userid>"
    When I remove a book with isbn "<isbn>" from my reading list with userId "<userid>"
    Then The book with isbn "<isbn>" is removed from account with userId "<userid>"

    Examples:
      | isbn          | userid                               |  |
      | 9781449325862 | e4ba0cad-5693-4f05-9978-213abe3c8bcc |  |

