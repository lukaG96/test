package org.wtec.testautomation.api.steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.wtec.testautomation.api.clients.AccountClient;
import org.wtec.testautomation.api.models.UserAuth;

public class AccountApiTests {
    private AccountClient accountClient;

    @Before
    public void testSetup() {
        accountClient = new AccountClient();
    }

    @Given("I am an authorized user {string} {string}")
    public void iAmAnAuthorizedUser(String username, String password) {
        accountClient.authorize(new UserAuth(username, password))
                .assertStatusCode(HttpStatus.SC_OK);
    }

    @Then("The book with isbn {string} is added to account with userId {string}")
    public void theBookIsAdded(String isbn, String userId) {
        var userBook = accountClient.getUserBook(userId, isbn);

        Assertions.assertThat(userBook).isNotNull();
    }

    @Then("The book with isbn {string} is removed from account with userId {string}")
    public void theBookIsRemoved(String isbn, String userId) {
        var userBook = accountClient.getUserBook(userId, isbn);

        Assertions.assertThat(userBook).isNull();
    }
}
