package org.wtec.testautomation.api.steps;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.wtec.testautomation.api.clients.BookStoreClient;
import org.wtec.testautomation.api.models.BookIsbn;
import org.wtec.testautomation.api.models.UserAddBooks;
import org.wtec.testautomation.api.models.UserReplaceBook;

import java.util.List;

public class BookStoreApiTests {
    private BookStoreClient bookStoreClient;

    @Before
    public void testSetup() {
        bookStoreClient = new BookStoreClient();
    }

    @Given("A list of books are available")
    public void aListOfBooksAreAvailable() {
        bookStoreClient.getBooks()
                .assertStatusCode(HttpStatus.SC_OK)
                .assertContentType(ContentType.JSON)
                .assertResponseBodyHasSize();
    }

    @When("I add a book with isbn {string} to my reading list with userId {string}")
    public void iAddABookToMyReadingList(String isbn, String userId) {
        bookStoreClient.addListOfBooks(new UserAddBooks(userId, List.of(new BookIsbn(isbn))))
                .assertStatusCode(HttpStatus.SC_CREATED);
    }

    @When("I remove a book with isbn {string} from my reading list with userId {string}")
    public void iRemoveABookFromMyReadingList(String isbn, String userId) {
        bookStoreClient.deleteBook(new UserReplaceBook(userId, isbn))
                .assertStatusCode(HttpStatus.SC_NO_CONTENT);
    }
}
