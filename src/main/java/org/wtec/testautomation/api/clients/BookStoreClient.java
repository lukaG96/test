package org.wtec.testautomation.api.clients;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpHeaders;
import org.wtec.testautomation.api.models.UserAddBooks;
import org.wtec.testautomation.api.models.UserReplaceBook;
import org.wtec.testautomation.core.api.BaseAPIClient;

public class BookStoreClient extends BaseAPIClient<BookStoreClient> {
    private final String ENDPOINT = "/bookstore/v1";

    @Getter
    @Setter
    private static String authorization;

    public BookStoreClient getBooks() {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .when()
                .get(ENDPOINT + "/Books")
        );
        return this;
    }

    public BookStoreClient addListOfBooks(UserAddBooks userBooks) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .contentType(ContentType.JSON)
                .body(userBooks)
                .when()
                .post(ENDPOINT + "/Books")
        );
        return this;
    }

    public BookStoreClient deleteAllBooks(String userId) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .queryParam(userId)
                .when()
                .delete(ENDPOINT + "/Books/" + userId)
        );
        return this;
    }

    public BookStoreClient replaceBook(String isbn, UserReplaceBook userBook) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .contentType(ContentType.JSON)
                .body(userBook)
                .when()
                .put(ENDPOINT + "/Books/" + isbn)
        );
        return this;
    }

    public BookStoreClient getBook(String isbn) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .queryParam(isbn)
                .when()
                .get(ENDPOINT + "/Book")
        );
        return this;
    }

    public BookStoreClient deleteBook(UserReplaceBook userBook) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .contentType(ContentType.JSON)
                .body(userBook)
                .when()
                .delete(ENDPOINT + "/Book")
        );
        return this;
    }
}
