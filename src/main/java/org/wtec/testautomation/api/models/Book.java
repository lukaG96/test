package org.wtec.testautomation.api.models;

import lombok.Data;

import java.util.Date;

@Data
public class Book {
    private final String isbn;
    private final String title;
    private final String subtitle;
    private final String author;
    private final Date publish_date;
    private final String publisher;
    private final Integer pages;
    private final String description;
    private final String website;
}
