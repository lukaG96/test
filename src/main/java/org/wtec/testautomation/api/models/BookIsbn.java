package org.wtec.testautomation.api.models;

import lombok.Data;

@Data
public class BookIsbn {
    private final String isbn;
}
