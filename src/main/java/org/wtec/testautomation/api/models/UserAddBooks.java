package org.wtec.testautomation.api.models;

import lombok.Data;

import java.util.List;

@Data
public class UserAddBooks {
    private final String userId;
    private final List<BookIsbn> collectionOfIsbns;
}
