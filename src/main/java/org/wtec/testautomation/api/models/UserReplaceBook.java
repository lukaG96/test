package org.wtec.testautomation.api.models;

import lombok.Data;

@Data
public class UserReplaceBook {
    private final String userId;
    private final String isbn;
}
