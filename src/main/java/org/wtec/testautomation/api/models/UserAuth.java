package org.wtec.testautomation.api.models;

import lombok.Data;

@Data
public class UserAuth {
    private final String userName;
    private final String password;
}
